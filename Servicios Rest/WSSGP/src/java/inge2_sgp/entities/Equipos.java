/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inge2_sgp.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author willians_ojeda
 */
@Entity
@Table(name = "equipos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Equipos.findAll", query = "SELECT e FROM Equipos e")
    , @NamedQuery(name = "Equipos.findByIdProyecto", query = "SELECT e FROM Equipos e WHERE e.equiposPK.idProyecto = :idProyecto")
    , @NamedQuery(name = "Equipos.findByIdEmpleado", query = "SELECT e FROM Equipos e WHERE e.equiposPK.idEmpleado = :idEmpleado")
    , @NamedQuery(name = "Equipos.findByDescripcion", query = "SELECT e FROM Equipos e WHERE e.descripcion = :descripcion")
    , @NamedQuery(name = "Equipos.findByEstado", query = "SELECT e FROM Equipos e WHERE e.estado = :estado")
    , @NamedQuery(name = "Equipos.findByUsrAlta", query = "SELECT e FROM Equipos e WHERE e.usrAlta = :usrAlta")
    , @NamedQuery(name = "Equipos.findByFecAlta", query = "SELECT e FROM Equipos e WHERE e.fecAlta = :fecAlta")
    , @NamedQuery(name = "Equipos.findByFecUltAct", query = "SELECT e FROM Equipos e WHERE e.fecUltAct = :fecUltAct")})
public class Equipos implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected EquiposPK equiposPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "estado")
    private String estado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "usr_alta")
    private String usrAlta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fec_alta")
    @Temporal(TemporalType.DATE)
    private Date fecAlta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fec_ult_act")
    @Temporal(TemporalType.DATE)
    private Date fecUltAct;
    @JoinColumn(name = "id_empleado", referencedColumnName = "id_empleado", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Empleados empleados;
    @JoinColumn(name = "id_proyecto", referencedColumnName = "id_proyecto", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Proyectos proyectos;

    public Equipos() {
    }

    public Equipos(EquiposPK equiposPK) {
        this.equiposPK = equiposPK;
    }

    public Equipos(EquiposPK equiposPK, String descripcion, String estado, String usrAlta, Date fecAlta, Date fecUltAct) {
        this.equiposPK = equiposPK;
        this.descripcion = descripcion;
        this.estado = estado;
        this.usrAlta = usrAlta;
        this.fecAlta = fecAlta;
        this.fecUltAct = fecUltAct;
    }

    public Equipos(int idProyecto, int idEmpleado) {
        this.equiposPK = new EquiposPK(idProyecto, idEmpleado);
    }

    public EquiposPK getEquiposPK() {
        return equiposPK;
    }

    public void setEquiposPK(EquiposPK equiposPK) {
        this.equiposPK = equiposPK;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getUsrAlta() {
        return usrAlta;
    }

    public void setUsrAlta(String usrAlta) {
        this.usrAlta = usrAlta;
    }

    public Date getFecAlta() {
        return fecAlta;
    }

    public void setFecAlta(Date fecAlta) {
        this.fecAlta = fecAlta;
    }

    public Date getFecUltAct() {
        return fecUltAct;
    }

    public void setFecUltAct(Date fecUltAct) {
        this.fecUltAct = fecUltAct;
    }

    public Empleados getEmpleados() {
        return empleados;
    }

    public void setEmpleados(Empleados empleados) {
        this.empleados = empleados;
    }

    public Proyectos getProyectos() {
        return proyectos;
    }

    public void setProyectos(Proyectos proyectos) {
        this.proyectos = proyectos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (equiposPK != null ? equiposPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Equipos)) {
            return false;
        }
        Equipos other = (Equipos) object;
        if ((this.equiposPK == null && other.equiposPK != null) || (this.equiposPK != null && !this.equiposPK.equals(other.equiposPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "inge2_sgp.entities.Equipos[ equiposPK=" + equiposPK + " ]";
    }
    
}
