/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inge2_sgp.entities.service;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author willians_ojeda
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(inge2_sgp.entities.service.ClientesFacadeREST.class);
        resources.add(inge2_sgp.entities.service.EmpleadosFacadeREST.class);
        resources.add(inge2_sgp.entities.service.EquiposFacadeREST.class);
        resources.add(inge2_sgp.entities.service.PersonasFacadeREST.class);
        resources.add(inge2_sgp.entities.service.ProyectosFacadeREST.class);
        resources.add(inge2_sgp.entities.service.RolesFacadeREST.class);
        resources.add(inge2_sgp.entities.service.TareasFacadeREST.class);
        resources.add(inge2_sgp.entities.service.UsuariosFacadeREST.class);
    }
    
}
