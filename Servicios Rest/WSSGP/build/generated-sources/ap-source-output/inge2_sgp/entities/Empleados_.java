package inge2_sgp.entities;

import inge2_sgp.entities.Equipos;
import inge2_sgp.entities.Personas;
import inge2_sgp.entities.Tareas;
import inge2_sgp.entities.Usuarios;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-08-14T11:36:45")
@StaticMetamodel(Empleados.class)
public class Empleados_ { 

    public static volatile SingularAttribute<Empleados, Date> fechaIngreso;
    public static volatile SingularAttribute<Empleados, String> estado;
    public static volatile CollectionAttribute<Empleados, Tareas> tareasCollection;
    public static volatile SingularAttribute<Empleados, Integer> idEmpleado;
    public static volatile CollectionAttribute<Empleados, Equipos> equiposCollection;
    public static volatile SingularAttribute<Empleados, Usuarios> usuario;
    public static volatile SingularAttribute<Empleados, String> cargo;
    public static volatile SingularAttribute<Empleados, Personas> idPersona;

}