package inge2_sgp.entities;

import inge2_sgp.entities.Empleados;
import inge2_sgp.entities.Proyectos;
import inge2_sgp.entities.TareasPK;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-08-14T11:36:45")
@StaticMetamodel(Tareas.class)
public class Tareas_ { 

    public static volatile SingularAttribute<Tareas, String> descripcion;
    public static volatile SingularAttribute<Tareas, String> estado;
    public static volatile SingularAttribute<Tareas, Date> fechaInicio;
    public static volatile SingularAttribute<Tareas, Empleados> idEmpleado;
    public static volatile SingularAttribute<Tareas, String> userAlta;
    public static volatile SingularAttribute<Tareas, TareasPK> tareasPK;
    public static volatile SingularAttribute<Tareas, Proyectos> proyectos;
    public static volatile SingularAttribute<Tareas, Date> fechaFin;
    public static volatile SingularAttribute<Tareas, Integer> prioridad;

}