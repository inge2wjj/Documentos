package inge2_sgp.entities;

import inge2_sgp.entities.Empleados;
import inge2_sgp.entities.EquiposPK;
import inge2_sgp.entities.Proyectos;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-08-14T11:36:45")
@StaticMetamodel(Equipos.class)
public class Equipos_ { 

    public static volatile SingularAttribute<Equipos, String> descripcion;
    public static volatile SingularAttribute<Equipos, String> usrAlta;
    public static volatile SingularAttribute<Equipos, String> estado;
    public static volatile SingularAttribute<Equipos, EquiposPK> equiposPK;
    public static volatile SingularAttribute<Equipos, Empleados> empleados;
    public static volatile SingularAttribute<Equipos, Date> fecAlta;
    public static volatile SingularAttribute<Equipos, Proyectos> proyectos;
    public static volatile SingularAttribute<Equipos, Date> fecUltAct;

}