package inge2_sgp.entities;

import inge2_sgp.entities.Personas;
import inge2_sgp.entities.Proyectos;
import inge2_sgp.entities.Usuarios;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-08-14T11:36:45")
@StaticMetamodel(Clientes.class)
public class Clientes_ { 

    public static volatile SingularAttribute<Clientes, String> ruc;
    public static volatile SingularAttribute<Clientes, String> razonSocial;
    public static volatile SingularAttribute<Clientes, Integer> idCliente;
    public static volatile CollectionAttribute<Clientes, Proyectos> proyectosCollection;
    public static volatile SingularAttribute<Clientes, String> direccion;
    public static volatile SingularAttribute<Clientes, Usuarios> usuario;
    public static volatile SingularAttribute<Clientes, String> telefono;
    public static volatile SingularAttribute<Clientes, Personas> idPersona;

}