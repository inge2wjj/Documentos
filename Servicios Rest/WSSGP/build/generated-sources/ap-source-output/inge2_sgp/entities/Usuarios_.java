package inge2_sgp.entities;

import inge2_sgp.entities.Clientes;
import inge2_sgp.entities.Empleados;
import inge2_sgp.entities.Roles;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-08-14T11:36:45")
@StaticMetamodel(Usuarios.class)
public class Usuarios_ { 

    public static volatile SingularAttribute<Usuarios, String> usrAlta;
    public static volatile SingularAttribute<Usuarios, Integer> estado;
    public static volatile SingularAttribute<Usuarios, String> password;
    public static volatile SingularAttribute<Usuarios, Roles> idRol;
    public static volatile CollectionAttribute<Usuarios, Empleados> empleadosCollection;
    public static volatile CollectionAttribute<Usuarios, Clientes> clientesCollection;
    public static volatile SingularAttribute<Usuarios, String> usuario;
    public static volatile SingularAttribute<Usuarios, Date> fecAlta;
    public static volatile SingularAttribute<Usuarios, Date> fecUltAct;

}