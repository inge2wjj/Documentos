package inge2_sgp.entities;

import inge2_sgp.entities.Clientes;
import inge2_sgp.entities.Equipos;
import inge2_sgp.entities.Tareas;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-08-14T11:36:45")
@StaticMetamodel(Proyectos.class)
public class Proyectos_ { 

    public static volatile SingularAttribute<Proyectos, String> descripcion;
    public static volatile SingularAttribute<Proyectos, Integer> idProyecto;
    public static volatile SingularAttribute<Proyectos, String> estado;
    public static volatile SingularAttribute<Proyectos, Clientes> idCliente;
    public static volatile CollectionAttribute<Proyectos, Tareas> tareasCollection;
    public static volatile SingularAttribute<Proyectos, Date> fechaInicio;
    public static volatile CollectionAttribute<Proyectos, Equipos> equiposCollection;
    public static volatile SingularAttribute<Proyectos, String> nombreProyecto;
    public static volatile SingularAttribute<Proyectos, Date> fechaFin;

}