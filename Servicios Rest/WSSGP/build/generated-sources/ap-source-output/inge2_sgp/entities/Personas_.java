package inge2_sgp.entities;

import inge2_sgp.entities.Clientes;
import inge2_sgp.entities.Empleados;
import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-08-14T11:36:45")
@StaticMetamodel(Personas.class)
public class Personas_ { 

    public static volatile SingularAttribute<Personas, Date> fechaNacimiento;
    public static volatile CollectionAttribute<Personas, Empleados> empleadosCollection;
    public static volatile SingularAttribute<Personas, BigInteger> cedula;
    public static volatile SingularAttribute<Personas, String> ciudad;
    public static volatile SingularAttribute<Personas, String> apellido;
    public static volatile SingularAttribute<Personas, String> correo;
    public static volatile SingularAttribute<Personas, String> direccion;
    public static volatile CollectionAttribute<Personas, Clientes> clientesCollection;
    public static volatile SingularAttribute<Personas, Integer> telefono;
    public static volatile SingularAttribute<Personas, String> sexo;
    public static volatile SingularAttribute<Personas, String> nombre;
    public static volatile SingularAttribute<Personas, Integer> idPersona;

}