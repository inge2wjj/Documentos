
CREATE SEQUENCE public.proyectos_id_proyecto_seq;

CREATE TABLE public.proyectos (
                id_proyecto VARCHAR NOT NULL DEFAULT nextval('public.proyectos_id_proyecto_seq'),
                nombre_proyecto VARCHAR NOT NULL,
                descripcion VARCHAR,
                cliente VARCHAR NOT NULL,
                fecha_inicio DATE,
                fecha_fin DATE,
                estado VARCHAR NOT NULL,
                CONSTRAINT proyectos_pk PRIMARY KEY (id_proyecto)
);


ALTER SEQUENCE public.proyectos_id_proyecto_seq OWNED BY public.proyectos.id_proyecto;

CREATE TABLE public.personas (
                id_persona NUMERIC NOT NULL,
                nombre VARCHAR NOT NULL,
                apellido VARCHAR NOT NULL,
                cedula NUMERIC NOT NULL,
                fecha_nacimiento DATE NOT NULL,
                telefono NUMERIC,
                ciudad VARCHAR,
                direccion VARCHAR,
                correo VARCHAR,
                CONSTRAINT personas_pk PRIMARY KEY (id_persona)
);


CREATE SEQUENCE public.empleados_id_empleado_seq;

CREATE TABLE public.empleados (
                id_empleado NUMERIC NOT NULL DEFAULT nextval('public.empleados_id_empleado_seq'),
                id_persona NUMERIC NOT NULL,
                fecha_ingreso DATE NOT NULL,
                CONSTRAINT empleados_pk PRIMARY KEY (id_empleado)
);


ALTER SEQUENCE public.empleados_id_empleado_seq OWNED BY public.empleados.id_empleado;

CREATE SEQUENCE public.tareas_id_tarea_seq;

CREATE TABLE public.tareas (
                id_tarea NUMERIC NOT NULL DEFAULT nextval('public.tareas_id_tarea_seq'),
                id_proyecto VARCHAR NOT NULL,
                id_empleado NUMERIC NOT NULL,
                descripcion VARCHAR NOT NULL,
                estado VARCHAR NOT NULL,
                fecha_inicio DATE,
                fecha_fin DATE,
                CONSTRAINT tareas_pk PRIMARY KEY (id_tarea, id_proyecto)
);


ALTER SEQUENCE public.tareas_id_tarea_seq OWNED BY public.tareas.id_tarea;

CREATE SEQUENCE public.roles_id_rol_seq;

CREATE TABLE public.roles (
                id_rol NUMERIC NOT NULL DEFAULT nextval('public.roles_id_rol_seq'),
                descripcion VARCHAR,
                nombre_rol VARCHAR NOT NULL,
                CONSTRAINT roles_pk PRIMARY KEY (id_rol)
);


ALTER SEQUENCE public.roles_id_rol_seq OWNED BY public.roles.id_rol;

CREATE TABLE public.usuarios (
                usuario VARCHAR NOT NULL,
                id_rol NUMERIC NOT NULL,
                id_empleado NUMERIC NOT NULL,
                estado NUMERIC DEFAULT 1 NOT NULL,
                CONSTRAINT usuarios_pk PRIMARY KEY (usuario)
);


ALTER TABLE public.tareas ADD CONSTRAINT proyectos_tareas_fk
FOREIGN KEY (id_proyecto)
REFERENCES public.proyectos (id_proyecto)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.empleados ADD CONSTRAINT personas_empleados_fk
FOREIGN KEY (id_persona)
REFERENCES public.personas (id_persona)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tareas ADD CONSTRAINT empleados_tareas_fk
FOREIGN KEY (id_empleado)
REFERENCES public.empleados (id_empleado)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.usuarios ADD CONSTRAINT empleados_usuarios_fk
FOREIGN KEY (id_empleado)
REFERENCES public.empleados (id_empleado)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.usuarios ADD CONSTRAINT roles_usuarios_fk
FOREIGN KEY (id_rol)
REFERENCES public.roles (id_rol)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;